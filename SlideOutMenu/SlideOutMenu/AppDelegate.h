//
//  AppDelegate.h
//  SlideOutMenu
//
//  Created by jameskrauser on 2018/5/17.
//  Copyright © 2018年 jameskrauser. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

