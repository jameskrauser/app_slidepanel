//
//  ViewController.m
//  SlideOutMenu
//
//  Created by jameskrauser on 2018/5/17.
//  Copyright © 2018年 jameskrauser. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

@synthesize sidPanel, menuBtn , menuBtn2 , transV;



- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapper =[[ UITapGestureRecognizer alloc ] initWithTarget:self action:@selector(hideSidePanel:) ];
    tapper.numberOfTapsRequired = 1;
    [transV addGestureRecognizer:tapper];
    
}


-(void)hideSidePanel:(UIGestureRecognizer *)gesture{
    NSLog(@"debug hideSIdePanel");
    if( gesture.state == UIGestureRecognizerStateEnded ){
        [transV setHidden:YES];
        [UIView transitionWithView:sidePanel duration:0.2
                           options:UIViewAnimationCurveEaseIn
                        animations:^{
                            CGRect frame = sidePanel.frame;
                            frame.origin.x = -sidePanel.frame.size.width;
                            sidePanel.frame = frame;
                        } completion:nil];
    }
}

-(IBAction)buttonPressed:(id)sender{
    NSLog(@"debug buttonPressed");
    if( sender == menuBtn ) {
        [transV setHidden:NO];
        [UIView transitionWithView:sidePanel duration:0.2
                           options:UIViewAnimationCurveEaseIn
                        animations:^{
                            CGRect frame = sidePanel.frame;
                            frame.origin.x = 0;
                            sidePanel.frame = frame;
                        } completion:nil];
        
    }
    else if (sender == menuBtn2 ) {
        [transV setHidden:YES];
        [UIView transitionWithView:sidePanel duration:0.2
                           options:UIViewAnimationCurveEaseIn
                        animations:^{
                            CGRect frame = sidePanel.frame;
                            frame.origin.x = -sidePanel.frame.size.width;
                            sidePanel.frame = frame;
                        } completion:nil];
    }
    
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
